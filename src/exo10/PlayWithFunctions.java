package exo10;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

public class PlayWithFunctions {

	public static void main(String[] args) {
		Function<String, String> toUpper = 
				s -> s.toUpperCase();
		Function<String, String> Identity = 
				s -> s == null ? "" :s;
		Function<String, Integer> toLength = 
				s -> s == null ? 0 : s.length();	
		Function<String, String> toParenthesis = 
				s -> s == null ? "()" : "(" + s + ")";
		BiFunction<String,String,Integer> indexOf =
				(s1 ,s2) -> s1.indexOf(s2);
		Function<String,Integer> indexOfAbcd =
				s -> indexOf.apply("abcdefghi",s);
				
		
		List <String> strings =
			Arrays.asList("","hi","abc","world");
		
		
		System.out.println("Identity function:");
		for (String s : strings) {
			System.out.println("\"" + s + "\" -> " + Identity.apply(s));
		}
		
		System.out.println("\ntoLength function:");
		for (String s : strings) {
			System.out.println( s + " -> " + toLength.apply(s));
		}
		
		System.out.println("\ntoParenthesis function:");
		for (String s : strings) {
			System.out.println( s + " -> " + toParenthesis.apply(s));
		}
		
		System.out.println("\nindexOfAbcd function:");
		for (String s : strings) {
			System.out.println( "\"abcdefghi\" index of " + s +  "\t-> " + 
					indexOfAbcd.apply(s));
		}
		
		System.out.println("\ntoUpper function:");
		for (String s : strings) {
			System.out.println( s + " -> " + toUpper.apply(s));
		}
		
		System.out.println("\n");
		System.out.println("\"Good morning\" index of \"morning\"\t" 
				+ indexOf.apply("Good morning","morning"));
		System.out.println("\"Hello\" index of \"World\"?\t" 
				+ indexOf.apply("Hello","word"));
	}

}
