package exo11;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class PlayWithLamda {

	public static void main(String[] args) {

		Person p1 = new Person("Courteney","Cox",20);
		Person p2 = new Person("Courteney","Cox",56);
		Person p3 = new Person("Jennifer","Aniston",52);
		Person p4 = new Person("Matt","LeBlanc",52);
		Person p5 = new Person("Matt ","Perry",51);
		Person p6 = new Person("Matthew","Perry",51);
		
		//Comparator<String> compareLength = (String s1, String s2) -> 
		//s1.length()-s2.length() == 0? 0: s1.length() - s2.length()<0?-1:1;
		Comparator<String> compareLength = 
				Comparator.comparing(String ::length);
		System.out.println("Comparing first names by length:");
		System.out.println(compareLength.compare(p1.getFistName(), p2.getFistName())); 
		System.out.println(compareLength.compare(p3.getFistName(), p4.getFistName())); 
		System.out.println(compareLength.compare(p4.getFistName(), p5.getFistName())); 
		
		Comparator<Person> compareLastName = 
				Comparator.comparing(t -> t.getLastName());
		System.out.println("\nComparing persons by last names:");
		System.out.println(compareLastName.compare(p1, p2)); 
		System.out.println(compareLastName.compare(p3, p4)); 
		System.out.println(compareLastName.compare(p4, p5)); 
		
		Comparator<Person> compareAge = 
				Comparator.comparing(Person::getAge);
		Comparator<Person> compareLastNameThenAge = 
				compareLastName.thenComparing(compareAge);
		System.out.println("\nComparing persons by last names then by age:");
		System.out.println(compareLastNameThenAge.compare(p1, p2)); 
		System.out.println(compareLastNameThenAge.compare(p3, p4)); 
		System.out.println(compareLastNameThenAge.compare(p5, p6)); 		
		
		Comparator<Person> compareAgeThenLastName = 
				compareAge.thenComparing(compareLastName);
		System.out.println("\nComparing persons by age then by last names:");
		System.out.println(compareAgeThenLastName.compare(p1, p2)); 
		System.out.println(compareAgeThenLastName.compare(p4, p3)); 
		System.out.println(compareAgeThenLastName.compare(p5, p6)); 	
		
		Comparator<Person> sortPerson = 
				Comparator.nullsLast(compareLastNameThenAge) ;
		List<Person> persons = Arrays.asList(p1,p2,p3,null,p4,p5,p6);
		persons.sort(sortPerson);
		for (Person person : persons) {
			System.out.println("� " + person);
		}
	}

}
