package exo9;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class PlayWithPredicate {

	public static void main(String[] args) {
		Predicate <String> pMoreThanFour = 
				(String s) -> s.length() > 4;
		Predicate <String> pNotEmpty = 
				(String s) -> s.length() != 0;
		Predicate <String> pStartWithJ = 
				(String s) -> s.startsWith("J");
		Predicate <String> pIsFive = 
				(String s) -> s.length() == 5;
		Predicate <String> pFiveJ = 
				pStartWithJ.and(pIsFive);
										
		List <String> strings =
				Arrays.asList("Messi","Iniesta","Javier","","Xavi","Jordi");
		
		for (String s : strings) {
			System.out.println("\"" + s + "\" is not empty ? " + pNotEmpty.test(s));
		}
		
		System.out.println("\n");		
		for (String s : strings) {
			System.out.println("\"" + s + "\" is more than four letters? " 
					+ pMoreThanFour.test(s) + "\t ,is he exactly five letters?"
			+ pIsFive.test(s));
		}
		
		System.out.println("\n");
		for (String s : strings) {
			System.out.println("\"" + s + "\" starts with \"J\"? " + pStartWithJ.test(s));
		}		
		
		System.out.println("\n");
		for (String s : strings) {
			System.out.println("\"" + s + "\" starts with \"J\" and is exactly five? "
					+ pFiveJ.test(s));
		}
	}

}
